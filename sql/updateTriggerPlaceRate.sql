DELIMITER $$

USE `thebest`$$

DROP TRIGGER /*!50032 IF EXISTS */ `placerateupdate`$$

CREATE
    /*!50017 DEFINER = 'root'@'localhost' */
    TRIGGER `placerateupdate` AFTER UPDATE ON `rating_of_place` 
    FOR EACH ROW BEGIN UPDATE place p SET p.rate=(SELECT AVG(mark) FROM rating_of_place WHERE id_place=p.id);END;
$$

DELIMITER ;