delimiter //
create trigger dishrate after insert on rating_of_dishes for each row begin update dishes d set d.rate=(select avg(mark) from rating_of_dishes where id_dish=d.id);end//