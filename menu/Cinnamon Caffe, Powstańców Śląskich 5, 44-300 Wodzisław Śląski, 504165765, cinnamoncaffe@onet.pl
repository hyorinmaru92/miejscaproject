﻿Kawa czarna z ekspresu;;Kawy;6
Biała kawa;;Kawy;7
Espresso;;Kawy;5
Espresso doppio (podwójne espresso);;Kawy;8
Espresso con panna (z dodatkiem bitej śmietany);;Kawy;7
Capuccino;;Kawy;7
Latte macchiato;;Kawy;10
Latte na podwójnym espresso;;Kawy;12
Latte z miodem i cynamonem;;Kawy;9.50
Frappe (mrożona kawa z bita smietaną);;Kawy;9
Frappe z lodami i bita śmietaną;;Kawy;10.50
Moccha (kawa z gorącą czekoladą);;Kawy;12
Americano;;Kawy;6
Bombardino (espresso z likierem jajecznym udekorowane bitą śmietaną);;Kawy;12
Irish coffee (czarna kawa z whisky Bushmills i bitą śmietaną);;Kawy;14
Kawa z mlekiem z dodatkiem syropu smakowego na dnie z bitą  śmietaną udekorowana smakową  posypką;;Kawy;12
Czekolada na gorąco;Napoje bezalkoholowe;Inne;10
Czekolada po wiedeńsku (gorąca czekolada z lodami waniliowymi i bitą śmietaną);Napoje bezalkoholowe;Inne;13
Czekoladowa fantazja (z 40 ml likieru jajecznego i bitą śmietaną);Napoje bezalkoholowe;Inne;12
Cynamonowa z pomarańczą (czekolada, syrop cynamonowy, cynamon, plaster pomarańczy);Napoje bezalkoholowe;Inne;9
Bananowa (czekolada, syrop bananowy, banan, bita śmietana);Napoje bezalkoholowe;Inne;9
Cubachoco (czekolada, syrop kokosowy, wiórki kokosowe, bita śmietana);Napoje bezalkoholowe;Inne;9
Czekolada z rumem i wiśniówką;Napoje bezalkoholowe;Inne;11
Mrożona z lodami (mrożona czekolada, gałka lodów waniliowych, bita śmietana);Napoje bezalkoholowe;Inne;11
Herbata Richmont czarna Ceylon, Mexican Dream ( z dodatkiem kawałków czekolady i chili);;Herbaty;7,350ml
Herbata Richmont zielona, zielona z miętą;;Herbaty;7,350ml
Herbata Richmont brzoskwiniowo-cytrynowa,  Pu – erh truskawkowa (czerwona), fruit ginger (owocowo-imbirowa);;Herbaty;7,350ml
Herbata z dodatkiem syropu imbirowego i cytryny;;Herbaty;8,350ml
Herbata rozgrzewająca z kawałkami imbiru;;Herbaty;9,350ml
Sok ze świeżo wyciskanych pomarańczy;;Soki;12
Sok ze świeżow wyciskanych grejfrutów;;Soki;12
Pomarańczowy ninja (świeża pomarańcza, truskawki, sok bananowy, sok z cytryny, kruszony lód);;Soki;12
Pink Velvet (słodka brzoskwinia, maliny, sok jabłkowy, sok z cytryny, kruszony lód);;Soki;12
Milkshakes (woce zmiksowane z mlekiem oraz lodami) truskawkowy, bananowy, brzoskwinioy, czekoladowy, waniliowy;Napoje bezalkoholowe;Inne;13
Jabłko z miętową nutą (soczyste jabłko, świeża mięta, sok tłoczony, cytryna i lód);;Koktajle;10
Banana Panama (banan, porcja espresso, gałka lodów, mleko i lód);;Koktajle;15
Szpinakowy Banan (szpinak, banan, listki mięty, sok tłoczony, sok z cytryny, lód);;Koktajle;10
Kropla beskidu gazowana;;Wody;4;200ml
Kropla beskidu niegazowana;;Wody;4;200ml
Coca- Cola;;Napoje gazowane;4.50;250ml
Coca Cola ZERO;;Napoje gazowane;4.50;250ml
Sprite;;Napoje gazowane;4.50;250ml
Fanta;;Napoje gazowane;4.50;250ml
Lemoniada (orzeźwiająca lemoniada na bazie marakui i świeżej cytryny, robiona według naszego oryginalnego przepisu);Napoje bezalkoholowe;Inne;8.50;200ml
Fritz-kola (jedna z najbardziej pobudzających koli świata, zawiera prawdziwy orzech koli oraz naturalna kofeinę);;Napoje gazowane;9.50;330ml
Yerbata (pobudzający i odświeżający napój na bazie mate);;Napoje;9.50;330ml
Wostok (orzeźwiający, ziołowy napój o aromacie lasów tajgi);;Napoje;9.50;330ml
Red Bull;Napoje bezalkoholowe;Inne;9.50,330ml
Tyskie Gronie z tanka;;Piwa;7;330ml
Tyskie Gronie z tanka;;Piwa;9;500ml
Pilsner Urquell Hladinka;;Piwa;8;330ml
Pilsner Urquell Hladinka;;Piwa;10;500ml
Pilsner Urquell Mliko;;Piwa;6.50
Pilsner Urquell Cochtan;Piwa;10
Pilsner Urquell Snyt;Piwa;8
Książęce lager czerwony;;Piwa;7;500ml
Książęce ciemne łagodne ;;Piwa;7;500ml
Książęce burgundowe trzy słody;;Piwa;7;500ml
Książęce pszeniczne;;Piwa;7;500ml
Lech;;Piwa;7;500ml
Tyskie;;Piwa;7;500ml
Desperados;;Piwa;7;400ml
Redd’s Pineapple, Redd’s Apple, Redd’s Cranberry;;Piwa;8;400ml
Grzane piwo podawane z przyprawami i sokiem malinowym, imbirowym i miodem;;Piwa;10;500ml
Likier kawowy Latte Macchiato;;Likiery;4;50ml
Likier porzeczkowy;;Likiery;5;50ml
Wino grzane korzenne;;Wina;12;200ml
Miód pitny;Alkohole;Inne;9;150ml
Miód pitny grzany;Alkohole;Inne;12;200ml
Red Spritz (likier z gorzkich pomarańczy z winem musującym);;Drinki;15
Citrus Spritz (likier z gorzkich pomarańczy z dodatkiem soków cytrusowych);;Drinki;14
Florida (likier z gorzkich pomarańczy z sokiem grejfrutowym i zimnym tonikiem);;Drinki;14
Florida Vincero (likier z gorzkich pomarańczy, sok grejfrutowy, wino musujące);;Drinki;16
Martini Soda (doskonały wermut z dodatkiem wody gazowanej i cząstki cytryny);;Drinki;10
Pina Colada (mix kokosa, soku ananasowego, rumu i śmietanki);;Drinki;13
Lodowy Anioł  (gałka lodów z sokiem pomarańczowym i winem musującym);;Drinki;13
Bull Royal (wino musujące z napojem energetyzującym);;Drinki;10
Szarlotka  na ciepło z bitą śmietaną i gałką lodów;;Ciasta;10
Ciasto czekoladowe z dodatkiem wiśni;;Ciasta;9
Sernik;;Ciasta;9
Peche Melba (brzoskwinie polane syropem waniliowym, ułożone na poduszce lodów waniliowych i przykryte puree z malin);Desery;Inne;18
Cynamonowa pokusa (gorący krem budyniowy przekładany biszkoptami korzennymi i jabłkami z cynamonem);Desery;Inne;10
Maliny na gorąco (z gałka lodów waniliowych i bitą śmietaną);Desery;Inne;14
Deser jogurtowy ( jogurt, owoce, musli);Desery;Inne;11
Kiwi frappe (kiwi, jogurt, gałka lodów śmietankowych);Desery;Inne;11 
Wiśniowa rozkosz (2 gałki lodów, gorąca czekolada, bita śmietana, wiśnie, posypka);;Lody;18
Czekoladowe marzenie (3 gałki lodów, bita śmietana, sos czekoladowy, dodatki);;Lody;15
Bakaliowy raj (2 gałki lodów, bita śmietana, sos toffi,bakalie);;Lody;16
Bananowy czar (2 gałki lodów, banany, bita śmietana, polewa czekolada, ajerkoniak);;Lody;14
Gofr z cukrem pudrem;;Gofry;4.50
Gofr z bitą śmietaną;;Gofry;6
Gofr z polewą (czekoladowa, toffi, truskawkowa, kiwi, orzechowa,  niebieska o smaku coli, Hello Sweety o smaku landrynki);;Gofry;5
Gofr z bitą śmietaną i polewą;;Gofry;7
Gofr z bitą śmietaną i musem owocowym;;Gofry;9
Gofr z bita śmietaną i owocami;;Gofry;10
Migdały prażone;;Słodkie;5
Oliwki Vincero;;Słone;7
Łosoś z avokado;;Słone;16
Bruscheta z pomidorami i świeżą bazylią;Przekąski;Inne;8
Caprese (pomidor przekładany mozzarellą, świeża bazylia z kremem balsamicznym, podane z opiekaną bagietka);Przekąski;Inne;14
Sałatka Vincero;Przekąski;Inne;18
Deska serów podana z opiekanym pieczywem;;Słone;20
Carpaccio (plasterki wołowiny  podane na rucoli z parmezanem,kaparami);Przekąski;Inne;25
Tost z szynką i oliwkami;Przekąski;Inne;10;2szt
Tost z pomidorem i mozarellą;Przekąski;Inne;10;2szt
Tost z szynką i ananasem;Przekąski;Inne;10;2szt
Vegetables sandwitch (grillowane warzywa, hummus, suszone pomidory, domowe frytki, mix sałat);Przekąski;Inne;19
Chorizo sandwich (hiszpańska kiełbasa chorizo fresco, pasta z pomidorów, grillowana papryka, rukola, czerwona cebula, domowe frytki, mix sałat);Przekąski;Inne;22
Halloumi burger (ser Halloumi, hummus, cebula karmelizowana, grillowana papryka, rukola, domowe frytki, sałatka coloslaw);Przekąski;Inne;24
Classic burger (200 g wołowiny, boczek, irlandzki Cheddar, cebula, pomidor, ogórek kiszony, majonez, domowe frytki, sałatka coleslaw);Przekąski;Inne;26

