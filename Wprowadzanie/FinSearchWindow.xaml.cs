﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for FinSearchWindow.xaml
    /// </summary>
    public partial class FinSearchWindow : Window
    {
        MySQLConnector connection = new MySQLConnector();
        DatabaseFunctions dbFunctions = new DatabaseFunctions();
        string imgName,place,city,address,userName;
        
        public FinSearchWindow(string lokal, string miasto, string adres, string login)
        {
            InitializeComponent();
            if (login == "")
            {
                cbRate.Visibility = System.Windows.Visibility.Hidden;
                btnRate.Visibility = System.Windows.Visibility.Hidden;
            }
            place = lokal;
            city = miasto;
            address = adres;
            userName = login;
            lokalLabel.Content = lokal+"\n"+miasto+" "+adres;
            try
            {
                ratingLabel.Content = connection.Select("select rate from place where name='" + place + "' and city='" + city + "' and address='" + address + "'")[0];
            }
            catch
            {
                ratingLabel.Content = "0";
            }
            int localID=dbFunctions.returnLocalID(lokal, adres);
            imgName= (dbFunctions.getLocalIMG(localID)).ToString();
            System.Data.DataTable table = new System.Data.DataTable();
            connection.DataAdapter(table, "select category.name,subcategory.name,dishes.name,dishes.price,dishes.size from category,subcategory,dishes,place where dishes.id_category = category.id and id_subcategory = subcategory.id and id_place=(select id from place where name='" + lokal + "' and city='"+miasto+"' and address='"+adres+"') group by dishes.name");
            lstMenu.ItemsSource = table.DefaultView;
            try
            {
                imageIMG.Source = new BitmapImage(new Uri(System.IO.Directory.GetCurrentDirectory() + "/../../LocalImages/" + imgName, UriKind.Absolute));
            }
            catch
            {
                imageIMG.Source = new BitmapImage(new Uri(System.IO.Directory.GetCurrentDirectory() + "/../../LocalImages/default.jpg", UriKind.Absolute));
            }   
        }
        private void clickRate(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(lstMenu.View.ToString());
        }
        private void clickPlaceRate(object sender, RoutedEventArgs e)
        {
            try
            {
                string test = "";
                test = connection.Select("select id from rating_of_place where id_user=(select id from users where login='" + userName + "') and id_place=(select id from place where name='" + place + "' and city='" + city + "' and address='" + address + "')")[0];
                string userId = connection.Select("select id from users where login='"+userName+"'")[0];
                string placeId = connection.Select("select id from place where name='"+place+"' and city='"+city+"' and address='"+address+"'")[0];
                connection.MySQLCommands("update rating_of_place set mark=" + (cbRate.SelectedValue as ComboBoxItem).Content + " where id_user =" + userId + " and id_place=" + placeId + "");
            }
            catch
            {
                string userId = connection.Select("select id from users where login='" + userName + "'")[0];
                string placeId = connection.Select("select id from place where name='" + place + "' and city='" + city + "' and address='" + address + "'")[0];
                connection.MySQLCommands("insert into rating_of_place (mark,comment,id_place,id_user) values (" + (cbRate.SelectedValue as ComboBoxItem).Content + ",'','" + placeId + "','" + userId + "')");
            }
            ratingLabel.Content = connection.Select("select rate from place where name='" + place + "' and city='" + city + "' and address='" + address + "'")[0];
        }
    }
}
