﻿using System;
using System.Windows;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace Wprowadzanie
{
   class MySQLConnector
   {
      private MySqlConnection connection;

      public MySQLConnector()
      {
         string conectingcommand = "SERVER=localhost;DATABASE=thebest;UID=root;PASSWORD=;CHARSET='utf8'";
         connection = new MySqlConnection(conectingcommand);
         try
         {
            connection.Open();
         }
         catch (MySqlException error)
         {
            MessageBox.Show(error.ToString());
         }
      }

      public List<string> Select (string query)
      {
         MySqlCommand cmd = new MySqlCommand (query,connection);
         MySqlDataReader reader = cmd.ExecuteReader();
         List<string> answers = new List<string>();
         string temp = "";
         while (reader.Read())
         {
             for (int i = 0; i < reader.FieldCount; i++)
             {
                 try
                 {
                     temp += reader.GetString(i);
                 }
                 catch
                 {
                     reader.Close();
                     return answers;
                 }
             }
            answers.Add(temp);
            temp = "";
         }
         reader.Close();
         return answers;
      }

      public void MySQLCommands(string query)
      {
          try
          {
              MySqlCommand cmd = new MySqlCommand(query, connection);
              cmd.ExecuteNonQuery();
          }
          catch
          {
          }
      }

      public System.Data.DataTable DataAdapter(System.Data.DataTable set, string query)
      {
         MySqlCommand cmd = new MySqlCommand(query, connection);
         MySqlDataAdapter adapter = new MySqlDataAdapter();
         adapter.SelectCommand = cmd;
         try
         {
             adapter.Fill(set);
         }
         catch
         {
         }
         return set;
      }
   }
}
