﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for MainWindowApp.xaml
    /// </summary>
    public partial class MainWindowApp : Window
    {
        MySQLConnector connection = new MySQLConnector();
        DatabaseFunctions dbFunctions = new DatabaseFunctions();
        TextBox CenaMinTextBox = new TextBox();
        TextBox CenaMaxTextBox = new TextBox();
        ComboBox KategoriaJedzComboBox = new ComboBox();
        ComboBox PodkatJedzComboBox = new ComboBox();
        Button WyszukajButton = new Button();
        TextBox nazwaDaniaTextBox = new TextBox();
        Button WylogujButton = new Button();
        Label loginLabel = new Label();
        Button DodajLokalButton = new Button();
        Button DodajMenuButton=new Button();
        Style style = Application.Current.FindResource("OnHoverButton") as Style;
        Label nazwadania_Label = new Label();
        Label cenaMin_Label = new Label();
        Label cenaMax_Label = new Label();
        Label kategoria_Label = new Label();
        Label podkategoria_Label = new Label();
        string login = "";
        bool isClicked = false;

        public MainWindowApp()
        {
            InitializeComponent();
            PrepareWindow();
            Button RejestracjaButton = new Button();
            RejestracjaButton.Content = "Rejestracja";
            RejestracjaButton.Margin = new Thickness(50, 0, 5, 0);
            Button LogowanieButton = new Button();
            LogowanieButton.Content = "Logowanie";
            LogowanieButton.Margin = new Thickness(0, 0, 15, 0);

            //przypisanie dzieci do StackPanelu górnego
            GornyStackPanel.Children.Add(RejestracjaButton);
            GornyStackPanel.Children.Add(LogowanieButton);

            //dodanie zdarzeń Click
            LogowanieButton.Click += LogowanieButtonClick;
            RejestracjaButton.Click += RejestracjaButtonClick;
            LogowanieButton.Style = style;
            RejestracjaButton.Style = style;

            //ustawienie poszczególnych wartości itemów
            RejestracjaButton.Width = 150;
            LogowanieButton.Width = 150;
            RejestracjaButton.Height = 35;
            LogowanieButton.Height = 35;
            RejestracjaButton.FontSize = 20; 
            LogowanieButton.FontSize = 20;
        }
        
        public MainWindowApp(string typUzytkownika,string login)
        {
            InitializeComponent();
            PrepareWindow();           
            if (typUzytkownika == "admin")
            {
                WylogujButton.Content = "Wyloguj";
                loginLabel.Content=login;
                loginLabel.FontSize = 20;
                loginLabel.FontWeight = FontWeights.Bold;
                loginLabel.Margin = new Thickness(20,30,0,0);
                DodajLokalButton.Content = "Dodaj lokal";
                DodajMenuButton.Content = "Dodaj menu";

                GornyStackPanel.Children.Add(DodajMenuButton);
                GornyStackPanel.Children.Add(DodajLokalButton);
                GornyStackPanel.Children.Add(WylogujButton);
                GornyStackPanel.Children.Add(loginLabel);

                //dodanie zewnętrznego stylu do przycisków
                WylogujButton.Style = style;
                DodajLokalButton.Style = style;
                DodajMenuButton.Style = style;

                WylogujButton.Width = 70;
                DodajLokalButton.Width = 100;
                DodajMenuButton.Width = 100;
                WylogujButton.Height = 30;
                DodajLokalButton.Height = 30;
                DodajMenuButton.Height = 30;

                DodajMenuButton.Margin = new Thickness(70, 0, 5, 0);
                DodajMenuButton.Click += DodajMenuButton_Click;
                DodajLokalButton.Click += DodajLokalButton_Click;
                WylogujButton.Click += WylogujButton_Click;

                this.login = login;
            }
            else
            {
                WylogujButton.Click += WylogujButton_Click;
                WylogujButton.Content = "Wyloguj";
                loginLabel.Content = typUzytkownika;
                WylogujButton.Style = style;

                GornyStackPanel.Children.Add(loginLabel);
                GornyStackPanel.Children.Add(WylogujButton);

                WylogujButton.Width = 70;
                loginLabel.Width = 70;
                WylogujButton.Height = 30;
                loginLabel.Height = 30;

            }

        }
        void PrepareWindow ()
        {
            MaxWidth = 950;
            MaxHeight = 650;
            Img_map.MaxHeight = 450;
            logoImg.Source = new BitmapImage(new Uri(System.IO.Directory.GetCurrentDirectory() + "/../../Images/logo.png", UriKind.Absolute));
            DrawMap(null,null);
            
            foreach (string miasto in connection.Select("select city from place group by city"))
            {
                MiastaComboBox.Items.Add(miasto);
            }

            foreach (string type in connection.Select("select name from type_of_place"))
            {
                TypLokComboBox.Items.Add(type);
            }

            DrawMap(null, null);

            //dodanie dzieci do panelu wyszukiwania zaawansowanego
            WyszZaawanStackPanel_Label.Children.Add(kategoria_Label);
            WyszZaawanStackPanel.Children.Add(KategoriaJedzComboBox);
            WyszZaawanStackPanel_Label.Children.Add(podkategoria_Label);
            WyszZaawanStackPanel.Children.Add(PodkatJedzComboBox);
            WyszZaawanStackPanel_Label.Children.Add(nazwadania_Label);
            WyszZaawanStackPanel.Children.Add(nazwaDaniaTextBox);
            WyszZaawanStackPanel_Label.Children.Add(cenaMin_Label);
            WyszZaawanStackPanel.Children.Add(CenaMinTextBox);
            WyszZaawanStackPanel_Label.Children.Add(cenaMax_Label);
            WyszZaawanStackPanel.Children.Add(CenaMaxTextBox);
            WyszZaawanStackPanel.Children.Add(WyszukajButton);
            WyszZaawanStackPanel_Label.Visibility = System.Windows.Visibility.Hidden;
            WyszZaawanStackPanel.Visibility = System.Windows.Visibility.Hidden;
        }
        void WylogujButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindowApp MainWindow = new MainWindowApp();
            MainWindow.Show();
            this.Close();
        }

        void DodajLokalButton_Click(object sender, RoutedEventArgs e)
        {
            AddLocal DodajLokalWindow = new AddLocal(login);
            DodajLokalWindow.Show();
        }

        void DodajMenuButton_Click(object sender, RoutedEventArgs e)
        {
            MainAddMenuWindow DodajMenuWindow = new MainAddMenuWindow(login);
            DodajMenuWindow.Show();
        }

        private void ZaawansowaneWysz_Click(object sender, RoutedEventArgs e)
        {
                if (!isClicked)
                {
                    isClicked = true;
                    KategoriaJedzComboBox.Items.Clear();

                    //przypisanie metod
                    WyszukajButton.Click += WyszukajButton_Click;
                    KategoriaJedzComboBox.SelectionChanged += PodkatJedzComboBox_SelectionChanged;
       
                    WyszukajButton.Content = "Wyszukaj";
                    kategoria_Label.Width = 180; 
                    nazwadania_Label.Width = 180; 
                    cenaMin_Label.Width = 180; 
                    cenaMax_Label.Width = 180; 
                    podkategoria_Label.Width = 180;

                    kategoria_Label.Content = "Kategoria";
                    podkategoria_Label.Content = "Podkategoria";
                    cenaMin_Label.Content = "Cena minimalna";
                    cenaMax_Label.Content = "Cena maksymalna";
                    nazwadania_Label.Content = "NazwaDania";

                    //wielkość itemów
                    WyszukajButton.Height = 30;
                    WyszukajButton.Width = 120;
                    KategoriaJedzComboBox.Width = 180;
                    PodkatJedzComboBox.Width = 180;
                    nazwaDaniaTextBox.Width = 180;
                    CenaMaxTextBox.Width = 180;
                    CenaMinTextBox.Width = 180;

                    nazwaDaniaTextBox.FontSize = 15;
                    WyszukajButton.FontSize = 15;
                    KategoriaJedzComboBox.FontSize = 15;
                    PodkatJedzComboBox.FontSize = 15;
                    CenaMinTextBox.FontSize = 15;
                    CenaMaxTextBox.FontSize = 15;
                    kategoria_Label.FontSize = 15;
                    podkategoria_Label.FontSize = 15;
                    nazwadania_Label.FontSize = 15;
                    cenaMin_Label.FontSize = 15;
                    cenaMax_Label.FontSize = 15;

                    //ustalenie marginesów
                    Thickness margines= new Thickness(5, 5, 50, 5);
                    KategoriaJedzComboBox.Margin = margines;
                    PodkatJedzComboBox.Margin = margines;
                    nazwaDaniaTextBox.Margin = margines;
                    CenaMaxTextBox.Margin = margines;
                    CenaMinTextBox.Margin = margines;
                    Thickness margines2 = new Thickness(5, 0, 0, 5);
                    kategoria_Label.Margin = margines2;
                    podkategoria_Label.Margin = margines2;
                    nazwadania_Label.Margin = margines2;
                    cenaMax_Label.Margin = margines2; 
                    WyszukajButton.Style = style;

                    foreach (string category in dbFunctions.getCategoriesNames())
                    {
                        KategoriaJedzComboBox.Items.Add(category);  
                    }
                   
                    WyszZaawanStackPanel.Visibility = System.Windows.Visibility.Visible;
                    WyszZaawanStackPanel_Label.Visibility = System.Windows.Visibility.Visible;
               
            }
            else
            {
                isClicked = false;
                WyszZaawanStackPanel.Visibility = System.Windows.Visibility.Hidden;
                WyszZaawanStackPanel_Label.Visibility = System.Windows.Visibility.Hidden; 
            }
        }

        void PodkatJedzComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           KategoriaJedzComboBox.IsEditable = false;
           PodkatJedzComboBox.IsEditable = false;
           PodkatJedzComboBox.Items.Clear();
           if (KategoriaJedzComboBox.SelectedItem == null) return;
           foreach (string podkategoria in dbFunctions.getSubCategoriesNames((sender as ComboBox).SelectedValue.ToString()))
           {
              PodkatJedzComboBox.Items.Add(podkategoria);
           }
        }


        void ProsteWysz_Click(object sender, RoutedEventArgs e)
        {
            System.Data.DataTable dane = new System.Data.DataTable();
            DatabaseFunctions dbFunctions = new DatabaseFunctions();
            string nazwaLokalu = NazwaLokTextBox.Text;
            string miasto = (MiastaComboBox.SelectionBoxItem.ToString() != "") ? MiastaComboBox.SelectedValue.ToString() : "";
            string typLokalu = (TypLokComboBox.SelectionBoxItem.ToString() != "") ? TypLokComboBox.SelectedValue.ToString() : "" ;
            
            connection.DataAdapter(dane, dbFunctions.getPlaces(nazwaLokalu,miasto,typLokalu));
            if (dane.Rows.Count > 0)
            {
                SearchNo2 searchWindow = new SearchNo2(sender, e, dane, login);
                searchWindow.Show();
            }
            else
                MessageBox.Show("Brak wyników");
        }
        void WyszukajButton_Click(object sender, RoutedEventArgs e)
        {
            System.Data.DataTable dane = new System.Data.DataTable();
            System.Data.DataTable dishesID = new System.Data.DataTable();
            DatabaseFunctions dbFunctions = new DatabaseFunctions();
            string nazwaLokalu, miasto, typ, kategoria, podkategoria, nazwaDania;

            nazwaLokalu = NazwaLokTextBox.Text;
            miasto = (MiastaComboBox.SelectionBoxItem.ToString() != "") ? MiastaComboBox.SelectedValue.ToString() : "";
            typ = (TypLokComboBox.SelectionBoxItem.ToString() != "") ? TypLokComboBox.SelectedValue.ToString() : "";
            kategoria = (KategoriaJedzComboBox.SelectionBoxItem.ToString() != "") ? KategoriaJedzComboBox.SelectedValue.ToString() : "";
            podkategoria = (PodkatJedzComboBox.SelectionBoxItem.ToString() != "") ? PodkatJedzComboBox.SelectedValue.ToString() : "" ;
            nazwaDania = nazwaDaniaTextBox.Text;
            connection.DataAdapter(dishesID, dbFunctions.getDishesID(nazwaLokalu, miasto, typ, kategoria, podkategoria, nazwaDania, CenaMinTextBox.Text, CenaMaxTextBox.Text));
            connection.DataAdapter(dane, dbFunctions.getDishes(nazwaLokalu,miasto,typ,kategoria,podkategoria,nazwaDania,CenaMinTextBox.Text,CenaMaxTextBox.Text));
            if (dane.Rows.Count > 0)
            {
                SearchNo1 SearchWindow = new SearchNo1(sender, e, dane, dishesID, login);
                SearchWindow.Show();
            }
            else
                MessageBox.Show("Brak wyników");
        }

        private void LogowanieButtonClick (object sender, RoutedEventArgs e)
        {
            Logowanie LogowanieWindow = new Logowanie();
            LogowanieWindow.Show();
            this.Close();
        }
        private void RejestracjaButtonClick(object sender, RoutedEventArgs e)
        {
            Registration RejestracjaWindow = new Registration();
            RejestracjaWindow.Show();
            this.Close();

        }
        private void DrawMap(object sender, MouseEventArgs e)
        {
            Img_map.Source = new BitmapImage(new Uri(System.IO.Directory.GetCurrentDirectory() + "/../../Images/mapa.png",UriKind.Absolute));
            Canvas.SetTop(Img_map, Img_map.Source.Height - 490);
            Canvas.SetLeft(Img_map, (500 / 2) - (Img_map.Source.Width / 2));
            while (Mapka.Children.Count != 1)
                Mapka.Children.Remove(Mapka.Children[1]);
            Ellipse Gliwice = new Ellipse
            {
                Height = 5,
                Width = 5,
                Stroke = Brushes.Black,
                Tag = "Gliwice",
                StrokeThickness = 3,
                Margin = new Thickness (170,245,0,0),
                ToolTip = new ToolTip(),
            };
            Ellipse Zabrze = new Ellipse
            {
                Height = 5,
                Width = 5,
                Stroke = Brushes.Black,
                Tag = "Zabrze",
                StrokeThickness = 3,
                Margin = new Thickness(180, 240, 0, 0),
                ToolTip = new ToolTip(),
            };
            Ellipse rSl = new Ellipse
            {
                Height = 5,
                Width = 5,
                Stroke = Brushes.Black,
                Tag = "Ruda Śląska",
                StrokeThickness = 3,
                Margin = new Thickness(190, 250, 0, 0),
            };
            Ellipse Pszczyna = new Ellipse
            {
                Height = 5,
                Width = 5,
                Stroke = Brushes.Black,
                Tag = "Pszczyna",
                StrokeThickness = 3,
                Margin = new Thickness(220, 305, 0, 0),
            };
            Ellipse wodzSl = new Ellipse
            {
                Height = 5,
                Width = 5,
                Stroke = Brushes.Black,
                Tag = "Wodzisław Śląski",
                StrokeThickness = 3,
                Margin = new Thickness(140, 315, 0, 0),
            };
            Label gliwiceLab = new Label
            {
                Content = "Gliwice",
                Margin = new Thickness(Gliwice.Margin.Left-45,Gliwice.Margin.Top-15,0,0),
            };
            Label zabrzeLab = new Label
            {
                Content = "Zabrze",
                Margin = new Thickness(Zabrze.Margin.Left - 40, Zabrze.Margin.Top - 20, 0, 0),
            };
            Label rSlLab = new Label
            {
                Content = "Ruda Śląska",
                Margin = new Thickness(rSl.Margin.Left+2, rSl.Margin.Top - 10, 0, 0),
            };
            Label PszczynaLab = new Label
            {
                Content = "Pszczyna",
                Margin = new Thickness(Pszczyna.Margin.Left + 2, Pszczyna.Margin.Top - 15, 0, 0),
            };
            Label wodzSlLab = new Label
            {
                Content = "Wodzisław Śląski",
                Margin = new Thickness(wodzSl.Margin.Left - 70, wodzSl.Margin.Top - 20, 0, 0),
            };
            Mapka.Children.Add(Gliwice);
            Mapka.Children.Add(gliwiceLab);
            Mapka.Children.Add(zabrzeLab);
            Mapka.Children.Add(Zabrze);
            Mapka.Children.Add(rSl);
            Mapka.Children.Add(rSlLab);
            Mapka.Children.Add(Pszczyna);
            Mapka.Children.Add(PszczynaLab);
            Mapka.Children.Add(wodzSl);
            Mapka.Children.Add(wodzSlLab);
            for (int i = 1; i < Mapka.Children.Count; i++)
            {
                (Mapka.Children[i]).MouseDown += DrawCityMap;
            }
        }

        private void DrawCityMap(object sender, MouseButtonEventArgs e)
        {
            if (sender is Ellipse)
                Mapka.Tag = (sender as Ellipse).Tag;
            else
                Mapka.Tag = (sender as Label).Content;
            Img_map.MouseDown -= DrawCityMap;
            Img_map.Source = new BitmapImage(new Uri(System.IO.Directory.GetCurrentDirectory() + "/../../Images/citymap.jpg", UriKind.Absolute));
            Canvas.SetLeft(Img_map, (500 / 2) - (Img_map.Source.Width / 2));
            while (Mapka.Children.Count != 1)
                Mapka.Children.Remove(Mapka.Children[1]);
            PointCollection points = new PointCollection()
            {
                new Point (30,35),
                new Point (0,45),
                new Point (30,55),
                new Point (20,45),
            };
            Polygon back = new Polygon
            {
                Points = points,
                Stroke = Brushes.Black,
                Fill = new SolidColorBrush(Colors.LightSlateGray),
            };
            Canvas.SetLeft(back, 30);
            Canvas.SetTop(back, 5);
            back.MouseDown += DrawMap;
            Mapka.Children.Add(back);
            System.Data.DataTable toprate = new System.Data.DataTable();
            if (sender is Ellipse)
            {
                Ellipse target = (sender as Ellipse);
                connection.DataAdapter(toprate,"select name,address from place where city like '" + target.Name + "%' order by rate desc limit 5");
            }
            else
            {
                Label target = (sender as Label);
                connection.DataAdapter(toprate,"select name,address from place where city like '" + target.Content + "%' order by rate desc limit 5");
            }
            double marginLeft = 0;
            double marginTop = 0;
            Random randomizer = new Random();
            if (sender is Ellipse)
            {
                Ellipse target = (sender as Ellipse);
                foreach (System.Data.DataRow local in toprate.Rows)
                {   
                    marginLeft = randomizer.NextDouble() * ((Img_map.Source.Width - 50) - 100) + 100;
                    marginTop = randomizer.NextDouble() * ((Img_map.Source.Height-50) - 50) + 50;
                    Ellipse lok = new Ellipse
                    {
                        Height = 10,
                        Width = 10,
                        Stroke = Brushes.Black,
                        StrokeThickness = 10,
                        Margin = new Thickness(marginLeft, marginTop, 0, 0),
                        ToolTip = local[1],
                        Tag = local[0],
                    };
                    lok.MouseDown += showResult;
                    Mapka.Children.Add(lok);
                }
            }
            else
            {
                Label target = (sender as Label);
                foreach (System.Data.DataRow local in toprate.Rows)
                {
                    marginLeft = randomizer.NextDouble() * ((Img_map.Source.Width - 50) - 100) + 100;
                    marginTop = randomizer.NextDouble() * ((Img_map.Source.Height-50) - 50) + 50;
                    Ellipse lok = new Ellipse
                    {
                        Height = 10,
                        Width = 10,
                        Stroke = Brushes.Black,
                        StrokeThickness = 10,
                        Margin = new Thickness(marginLeft, marginTop, 0, 0),
                        ToolTip = local[0],
                        Tag = local[1],
                    };
                    lok.MouseDown += showResult;
                    Mapka.Children.Add(lok);
                }
            }
        }

        private void showResult(object sender, MouseButtonEventArgs e)
        {
            FinSearchWindow results = new FinSearchWindow((sender as Ellipse).ToolTip.ToString(), ((sender as Ellipse).Parent as Canvas).Tag.ToString(), (sender as Ellipse).Tag.ToString(), login);
            results.Show();
        }
    }
}
