﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for SearchNo2.xaml
    /// </summary>
    public partial class SearchNo2 : Window
    {
        string userName;
        private DataTable _dataTable;
        public SearchNo2(object sender, RoutedEventArgs e, DataTable dane, string login)
        {
            InitializeComponent();
            MaxHeight=500;
            userName = login;
            MaxWidth=1000;
            Window_Loaded(sender, e, dane);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e, DataTable dane)
        {
            _dataTable = dane;
            ListView2.ItemsSource = dane.DefaultView;
        }
       
        void Przejdz_btn_Click(object sender, RoutedEventArgs e)
        {
            int clickedRow = 0;
            clickedRow = ListView2.Items.IndexOf((sender as FrameworkElement).DataContext);
            string lokal = (_dataTable.Rows[clickedRow][0]).ToString();
            string miasto = (_dataTable.Rows[clickedRow][1]).ToString();
            string address = (_dataTable.Rows[clickedRow][2]).ToString();
            FinSearchWindow finSearchWindow = new FinSearchWindow(lokal, miasto, address, userName);
            finSearchWindow.Show();
        }
        
    }
}

