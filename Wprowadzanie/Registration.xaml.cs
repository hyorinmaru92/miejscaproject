﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Wprowadzanie
{
    
    public partial class Registration : Window
    {
        string typKonta = "";

        private static bool IsValidEmail(string email)
        {
            const string validationRegex = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
            return new Regex(validationRegex, RegexOptions.IgnoreCase).IsMatch(email);
        }

        private static bool IsValidPhoneNumber(string phoneNumber)
        {
            const string validationRegex = @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$";
            return new Regex(validationRegex, RegexOptions.IgnoreCase).IsMatch(phoneNumber);
        }

        private static bool IsValidName(string name)
        {
            const string validationRegex = @"^[A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ][A-Za-zżźćńółęąśŻŹĆĄŚĘŁÓŃ]*$";
            return new Regex(validationRegex, RegexOptions.IgnoreCase).IsMatch(name);
        }

        private static bool IsValidCityName(string cityName)
        {
            const string validationRegex = @"^([a-zA-ZŻŹĆĄŚĘŁÓŃ]+|[a-zA-żźćńółęąśZŻŹĆĄŚĘŁÓŃ]+\s[a-zA-żźćńółęąśZŻŹĆĄŚĘŁÓŃ]+)$";
            return new Regex(validationRegex, RegexOptions.IgnoreCase).IsMatch(cityName);
        }

        public Registration()
        {
            Style style = Application.Current.FindResource("SmallWindow") as Style;
            this.Style = style;
            InitializeComponent();
            MaxHeight = 600;
            MaxWidth = 400;

            Button CofnijButton = new Button();
            CofnijButton.Click += CofnijButton_Click;
            CofnijButton.Content = "Cofnij";
            CofnijButton.Margin = new Thickness(10);

            buttonWrapPanel.Children.Add(CofnijButton);

            Button RejestrujButton = new Button();
            RejestrujButton.Click += RejestrujButton_Click;
            RejestrujButton.Content = "Zatwierdź";
            RejestrujButton.Margin = new Thickness(10);

            buttonWrapPanel.Children.Add(RejestrujButton);
        }

        void RejestrujButton_Click(object sender, RoutedEventArgs e)
        {
            if (typKonta == "")
                MessageBox.Show("Wybierz typ konta!");
            else
            {
                if (loginTextBox.Text.Length == 0)
                {
                    MessageBox.Show("Wpisz login!");
                }
                else
                {
                    if (powtHasloTextBox.Password == "" && hasloTextBox.Password == "")
                        MessageBox.Show("Wpisz hasło!");
                    else if (hasloTextBox.Password != powtHasloTextBox.Password )
                    {
                        MessageBox.Show("Niepoprawnie powtórzone hasło!");
                        hasloTextBox.Clear();
                        powtHasloTextBox.Clear();
                    
                    }
                    else
                    {
                        if (imieTextBox.Text.Length == 0)
                        {
                            MessageBox.Show("Wpisz imię!");
                        }
                        else if (imieTextBox.Text.Length > 0 && IsValidName(imieTextBox.Text) == false)
                        {
                            MessageBox.Show("Niepoprawne imię");
                        }
                        else
                        {
                            if (nazwiskoTextBox.Text.Length == 0)
                            {
                                MessageBox.Show("Wpisz nazwisko!");
                            }
                            else if (nazwiskoTextBox.Text.Length > 0 && IsValidName(nazwiskoTextBox.Text) == false)
                            {
                                MessageBox.Show("Niepoprawne nazwisko");
                            }
                            else
                            {
                                if (miastoTextBox.Text.Length == 0)
                                {
                                    MessageBox.Show("Wpisz nazwę miasta!");
                                }
                                else if (miastoTextBox.Text.Length > 0 && IsValidCityName(miastoTextBox.Text) == false)
                                {
                                    MessageBox.Show("Niepoprawna nazwa miasta");
                                }
                                else
                                {
                                    if (emailTextBox.Text.Length == 0)
                                    {
                                        MessageBox.Show("Wpisz email!");
                                    }
                                    else if (emailTextBox.Text.Length > 0 && IsValidEmail(emailTextBox.Text) == false)
                                    {
                                        MessageBox.Show("Niepoprawny format adresu e-mail. Przykładowy to nazwa@domena.pl");
                                    }
                                    else
                                    {
                                        if (telefonTextBox.Text.Length == 0)
                                        {
                                            MessageBox.Show("Wpisz numer telefonu!");
                                        }
                                        else if (telefonTextBox.Text.Length > 0 && telefonTextBox.Text.Length < 13 && IsValidPhoneNumber(telefonTextBox.Text) == false)
                                        {
                                            MessageBox.Show("Niepoprawny format numeru telefonu. Przykładowy to +48660123123");
                                        }
                                        else
                                        {
                                            MySQLConnector connection = new MySQLConnector();
                                            try
                                            {
                                                string test=connection.Select("select login from users where login='"+loginTextBox.Text+"'")[0];
                                                MessageBox.Show("Podany login jest juz zajęty");
                                                return;
                                            }
                                            catch
                                            {
                                                MainWindowApp MainWindow = new MainWindowApp();
                                                connection.MySQLCommands("insert into users(login,password,name,surname,telephone,email) values('" + loginTextBox.Text + "','" + hasloTextBox.Password + "','" + imieTextBox.Text + "','" + nazwiskoTextBox.Text + "'," + telefonTextBox.Text + ",'" + emailTextBox.Text + "')");
                                                if (typKonta == "właściciel lokalu")
                                                {
                                                    string User = connection.Select("select id from users where login='" + loginTextBox.Text + "'")[0];
                                                    connection.MySQLCommands("insert into admins(id_user) values (" + User + ")");
                                                }
                                                MainWindow.Show();
                                                this.Close();
                                                MessageBox.Show(" Rejestracja zakończona! \n Zaloguj się");
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
            }
        }
        void CofnijButton_Click(object sender, RoutedEventArgs e)
        {
            MainWindowApp MainWindow = new MainWindowApp();
            MainWindow.Show();
            this.Close();
        }

        private void TypKontaComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            typKonta = ((ComboBoxItem)((sender as ComboBox).SelectedItem)).Content.ToString();
        }

    }
}
