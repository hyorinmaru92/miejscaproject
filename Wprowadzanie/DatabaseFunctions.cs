﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wprowadzanie
{
    class DatabaseFunctions
    {
        MySQLConnector dbConnector = new MySQLConnector();
        public DatabaseFunctions()
        {
        }

        public string insertValue(string query, int localID)
        {
            try
            {
                string answer = "";
                string[] args = query.Split(';');
                string category, size;
                int length = args.Length;
                if (args.Length == 4)
                    size = "standard";
                else
                    size = args[4];
                List<string> isCategoryCorrect = dbConnector.Select("select id from category where name='" + args[1] + "'");
                if (isCategoryCorrect.Count == 1 && dbConnector.Select("select id from subcategory where id_category='" + isCategoryCorrect[0] + "' and name='" + args[2] + "'").Count == 1)
                    category = args[1];
                else
                    category = dbConnector.Select("select name from category where id=ANY( select id_category from subcategory where name='" + args[2] + "')")[0];
                string sth = dbConnector.Select("select insertRecord('" + args[0] + "', '" + category + "', '" + args[2] + "', " + args[3].Replace(',', '.') + ", '" + size + "', " + localID + " ) ")[0];
                return answer;
            }
            catch(Exception e)
            {
                System.Windows.MessageBox.Show("Funkcja dodająca menu zgłasza błąd:"+e.ToString()+"");
                return "";
            }
        }

        public string getDishes(string localName, string cityName, string localType, string categoryName, string subCategoryName, string dishName, string minPrice, string maxPrice)
        {
            string dishes = "select dishes.name as Nazwa,size as Porcja, price as Cena, category.name as Kategoria, subcategory.name as Podkategoria from dishes, category, subcategory where if('" + dishName + "'='',dishes.name not like '" + dishName + "',dishes.name='" + dishName + "')" +
                "and if( '" + localName + "'='', id_place not like '" + localName + "', id_place=ANY(select id from place where name='" + localName + "') )" +
                "and if( '" + cityName + "'='', id_place not like '" + cityName + "', id_place=ANY(select id from place where city='" + cityName + "') )" +
                "and if( '" + localType + "'='', id_place not like '" + localType + "', id_place=ANY(select id from place where id_type_place=(select id from type_of_place where name='" + localType + "')) )" +
                "and if( '" + categoryName + "'='', dishes.id_category not like '" + categoryName + "', dishes.id_category=( select id from category where category.name='" + categoryName + "') )" +
                "and if( '" + subCategoryName + "'='', dishes.id_subcategory not like '" + subCategoryName + "', dishes.id_subcategory=( select id from subcategory where subcategory.name='" + subCategoryName + "') )" +
                "and if( '" + minPrice + "'='', price not like '" + minPrice + "', price>'" + minPrice + "' )" +
                "and if( '" + maxPrice + "'='', price not like '" + maxPrice + "', price<'" + maxPrice + "' )" + 
                "and dishes.id_category=category.id and dishes.id_subcategory=subcategory.id order by rate desc";
            return dishes;
        }

        public string getDishesID(string localName, string cityName, string localType, string categoryName, string subCategoryName, string dishName, string minPrice, string maxPrice)
        {
            string dishes = "select id_place from dishes, category, subcategory where if('" + dishName + "'='',dishes.name not like '" + dishName + "',dishes.name='" + dishName + "')" +
                "and if( '" + localName + "'='', id_place not like '" + localName + "', id_place=ANY(select id from place where name='" + localName + "') )" +
                "and if( '" + cityName + "'='', id_place not like '" + cityName + "', id_place=ANY(select id from place where city='" + cityName + "') )" +
                "and if( '" + localType + "'='', id_place not like '" + localType + "', id_place=ANY(select id from place where id_type_place=(select id from type_of_place where name='" + localType + "')) )" +
                "and if( '" + categoryName + "'='', dishes.id_category not like '" + categoryName + "', dishes.id_category=( select id from category where category.name='" + categoryName + "') )" +
                "and if( '" + subCategoryName + "'='', dishes.id_subcategory not like '" + subCategoryName + "', dishes.id_subcategory=( select id from subcategory where subcategory.name='" + subCategoryName + "') )" +
                "and if( '" + minPrice + "'='', price not like '" + minPrice + "', price>'" + minPrice + "' )" +
                "and if( '" + maxPrice + "'='', price not like '" + maxPrice + "', price<'" + maxPrice + "' )" +
                "and dishes.id_category=category.id and dishes.id_subcategory=subcategory.id order by rate desc";
            return dishes;
        }

        public string getPlaces(string placeName, string city, string type)
        {
            string places = "select place.name as Nazwa, city as Miasto, address as Adres, telephone as Telefon, email as 'E-Mail', type_of_place.name as Typ, rate as Ocena from place, type_of_place where " +
                "if('" + placeName + "'='', place.name not like '" + placeName + "', place.name='" + placeName + "')" +
                "and if('" + city + "'='', place.city not like '" + city + "',place.city='" + city + "')" +
                "and if('" + type + "'='', id_type_place not like 0, id_type_place=(select id from type_of_place where name='" + type + "'))" +
                "and place.id_type_place=type_of_place.id order by rate desc"
                ;
            return places;
        }

        public void addComment( string comment, int localID, int mark, int userID )
        {
            dbConnector.Select("insert into rating_of_place values (0," + mark + ",'" + comment + "', " + localID + ", " + userID + " )");
        }

        public void rateDish( int dishID, int mark, int userID)
        {
            dbConnector.Select("insert into rating_of_dishes values (0," + mark + "," + userID + ", " + dishID + " )");
        }

        public int returnLocalID(string name, string city)
        {
            int localID;
            try
            {
                localID = Convert.ToInt32( dbConnector.Select("select id from place where name='" + name + "' and city='" + city + "' " )[0] );
            }
            catch
            {
                return 0;
            }
            return localID;
        }

        public System.Data.DataTable returnUserPlaces(string login)
        {
            System.Data.DataTable places = new System.Data.DataTable();
            string userID = dbConnector.Select("select id from users where login='"+login+"'")[0];
            dbConnector.DataAdapter(places,"select name,city from place where id=any(select id_place from admins where id_user="+userID+")");
            return places;
        }

        public string getLocalIMG(int localID)
        {
            string url = "";
            try
            {
                url = dbConnector.Select("select img from place where id='" + localID + "'")[0];
            }
            catch
            {
                return url;
            }
            return url;
        }

        public List<string> getCategoriesNames()
        {
            List<string> categoriesNames = new List<string>();
            categoriesNames = dbConnector.Select("select name from category");
            return categoriesNames;
        }

        public List<string> getSubCategoriesNames(string category)
        {
            List<string> subCategoriesNames = new List<string>();
            subCategoriesNames = dbConnector.Select("select name from subcategory where id_category = (select id from category where name='"+category+"')");
            return subCategoriesNames;
        }
        public string returnCategoryID(string kategoria)
        {
            return dbConnector.Select("select id from type_of_place where name = '" + kategoria + "'")[0];
        }
    }
}
