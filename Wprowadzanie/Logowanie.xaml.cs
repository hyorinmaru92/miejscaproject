﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for Logowanie.xaml
    /// </summary>
    public partial class Logowanie : Window
    {
        string User = "";
        string Password = "";
        MySQLConnector database = new MySQLConnector();

        public Logowanie()
        {
            InitializeComponent();
        }

        private void Loguj_Click(object sender, RoutedEventArgs e)
        {
            User = loginTextBox.Text.ToString();
            string UserExist = CzyIstnieje(User);

            if (loginTextBox.Text.Length == 0)
            {
                MessageBox.Show("Podaj login!");
            }
            else
            {
                if (UserExist != "")
                {
                    Password = haslo.Password.ToString();
                    if (Password != UserExist)
                    {
                        MessageBox.Show("Nie poprawne hasło!");
                    }
                    else
                    {
                        string TypeUser = TypUzytkownika(User);
                        MainWindowApp MainWindow = new MainWindowApp(TypeUser, User);
                        MainWindow.Show();
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Nie ma takiego użytkownika!");
                }
            }
        }

        string CzyIstnieje(String NameUser)
        {
             string sql="Select password from users where login='" +NameUser+"';";
            List<string> result = new List<string>();
             result=database.Select(sql);
             if (result.Count == 0)
             {
                 return "";
             }
             else
            return result[0];
         }

         string TypUzytkownika(String NameUser)
         {
             string sql = "select id from admins where id_user=(select id from users where login='"+ NameUser + "')";
             List<string> result = new List<string>();
             result = database.Select(sql);
             if (result.Count == 0)
             {
                 return "user";
             }
             else
                 return "admin";
         }

         private void Cofnij_Click(object sender, RoutedEventArgs e)
         {
             MainWindowApp mainWindow = new MainWindowApp();
             mainWindow.Show();
             this.Close();
         }
    }
}
