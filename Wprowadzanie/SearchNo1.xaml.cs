﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for SearchNo1.xaml
    /// </summary>
    ///

    public partial class SearchNo1 : Window
    {
        string userName;
        private DataTable dishesid;
        public SearchNo1(object sender, RoutedEventArgs e, DataTable dane, DataTable dishesID, string login)
        {
            MaxHeight = 500;
            MaxWidth = 1000;
            userName = login;
            InitializeComponent();
            Window_Loaded(sender, e, dane, dishesID);
        }

        private void Window_Loaded(object sender, RoutedEventArgs e, DataTable dane, DataTable dishesID)
        {
            dishesid = dishesID;
            ListView1.ItemsSource = dane.DefaultView;
        }

        void lokalizuj_btn_Click(object sender, RoutedEventArgs e)
        {
            int clickedRow = 0;
            clickedRow = ListView1.Items.IndexOf((sender as FrameworkElement).DataContext);
            MySQLConnector connection = new MySQLConnector();
            string lokal = connection.Select("select name from place where id=" + dishesid.Rows[clickedRow][0].ToString()+"")[0];
            string miasto = connection.Select("select city from place where id=" + dishesid.Rows[clickedRow][0].ToString() + "")[0];
            string address = connection.Select("select address from place where id=" + dishesid.Rows[clickedRow][0].ToString() + "")[0];
            FinSearchWindow finSearchWindow = new FinSearchWindow(lokal, miasto, address, userName);
            finSearchWindow.Show();
        }
    }
}
