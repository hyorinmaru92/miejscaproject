﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for AddLocal.xaml
    /// </summary>
    public partial class AddLocal : Window
    {
        MySQLConnector connection = new MySQLConnector();
        DatabaseFunctions databaseFunctions = new DatabaseFunctions();
        string nazwaLokalu=" ";
        string adresL=" ";
        string numerTL=" ";
        string emailL=" ";
        string imgL=" ";
        string typL=" ";
        string miasto = " ";
        string filename = "";
        string userName = "";
        public AddLocal(string user)
        {
            InitializeComponent();
            MaxHeight = 550;
            MaxWidth = 500;
            userName = user;
        }

        private static bool IsValidName(string name)
        {
            if (name == "")
                return false;
            const string validationRegex = @"^([a-zA-ZŻŹĆĄŚĘŁÓŃ]+|[a-zA-żźćńółęąśZŻŹĆĄŚĘŁÓŃ]+\s[a-zA-żźćńółęąśZŻŹĆĄŚĘŁÓŃ]+)$";
            return new Regex(validationRegex, RegexOptions.IgnoreCase).IsMatch(name);
        }

        private static bool IsValidPhoneNumber(string phoneNumber)
        {
            if (phoneNumber == "")
                return false;
            const string validationRegex = @"^\+?(\d[\d-. ]+)?(\([\d-. ]+\))?[\d-. ]+\d$";
            return new Regex(validationRegex, RegexOptions.IgnoreCase).IsMatch(phoneNumber);
        }

        private static bool IsValidEmail(string email)
        {
            if (email == "")
                return false;
            const string validationRegex = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|" + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)" + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";
            return new Regex(validationRegex, RegexOptions.IgnoreCase).IsMatch(email);
        }

        private void ZatwierdzLokalButton(object sender, RoutedEventArgs e)
        {

            if (nazwaTextBox.Text.Length == 0)
            {
                MessageBox.Show("Podaj nazwę lokalu!");
                return;
            }
            else
            {
                if (!IsValidName(miastoTextBox.Text))
                {
                    MessageBox.Show("Niepoprawna nazwa miasta");
                    return;
                }
                else
                {
                    if (adresTextBox.Text == "")
                    {
                        MessageBox.Show("Wypełnij ulicę");
                        return;
                    }
                    else
                    {
                        if (IsValidPhoneNumber(telefonTextBox.Text) == false)
                        {
                            MessageBox.Show("Niepoprawny format numeru telefonu. Przykładowy to +48660123123");
                            return;
                        }
                        else
                        {
                            if (IsValidEmail(emailTextBox.Text) == false)
                            {
                                MessageBox.Show("Niepoprawny format adresu e-mail. Przykładowy to nazwa@domena.pl");
                                return;
                            }
                            else
                            {
                                this.Close();
                            }
                        }
                    }
                }
            }
            nazwaLokalu = nazwaTextBox.Text;
            miasto = miastoTextBox.Text;
            emailL = emailTextBox.Text;
            adresL = adresTextBox.Text;
            numerTL = telefonTextBox.Text;
            imgL = zdjecieTextBox.Text;
            string[] tab = imgL.Split(new Char[] { '\\' });
            string exactfilename = tab[(tab.Length) - 1];
            string kategoria = TypLokComboBox.SelectionBoxItem.ToString();
            string id = databaseFunctions.returnCategoryID(kategoria);
            connection.MySQLCommands("insert into place(name, address, telephone, email, img, id_type_place,city) values( '" + nazwaLokalu + "','" + adresL + "'," + numerTL + ", '" + emailL + "', '" + exactfilename + "'," + id + ", '" + miasto + "'  )");
            try
            {
                string test = "";
                test = connection.Select("select id_place from admins where id_user=(select id from users where login='" + userName + "')")[0];
                connection.MySQLCommands("insert into admins(id_place,id_user) values (" + connection.Select("select id from place where name='" + nazwaLokalu + "' and address='" + adresL + "'")[0] + "," + connection.Select("select id from users where login='" + userName + "'")[0] + ")");
            }
            catch
            {
                connection.MySQLCommands("update admins set id_place=" + connection.Select("select id from place where name='" + nazwaLokalu + "' and address='" + adresL + "'")[0] + " where id_user=" + connection.Select("select id from users where login='" + userName + "'")[0] + "");
            }
            if (!System.IO.Directory.Exists(System.IO.Path.GetFullPath(filename + "/../../LocalImages/")))
            {
                System.IO.Directory.CreateDirectory( System.IO.Path.GetFullPath(filename + "/../../LocalImages/"));
            }
            try
            {
                System.IO.File.Copy(filename, System.IO.Path.GetFullPath(filename + "/../../LocalImages/" + exactfilename));
            }
            catch
            {
            }
            this.Close();
        }

        private void WybPlik_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
            dlg.DefaultExt = ".txt";
            dlg.Filter = "JPG Files (*.jpg)|*.jpg";
                
            // Display OpenFileDialog by calling ShowDialog method             
            Nullable<bool> result = dlg.ShowDialog();
            // Get the selected file name and display in a TextBox 
            if (result == true)
            {
                filename = dlg.FileName;
                zdjecieTextBox.Text = filename;
            }
        }

        private void TypLokComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TypLokComboBox.IsEditable = false;
        }
    }
}
