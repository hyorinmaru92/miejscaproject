﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for AddProductGroup.xaml
    /// </summary>
    
    public partial class AddProductGroup : Window   
    {
        public string podkategoria = "";
        string[] podkategorie = new string[0];
        string ButtonTaglokalny = "";
        bool isDone = false;
        MainAddMenuWindow main;
        DatabaseFunctions dbFunctions = new DatabaseFunctions();
        MySQLConnector connection = new MySQLConnector();

        public AddProductGroup(string ButtonTag,string lokal, MainAddMenuWindow window)
        {
            main = window;
            InitializeComponent();
            lokalLabel.Content = lokal;
            kolejnaPozycjaButton.IsEnabled = false;
            MaxHeight = 500;
            MaxWidth = 510;
            jakaKategoria.Content = ButtonTag;
            ButtonTaglokalny = ButtonTag;
            foreach (string podkategoria in dbFunctions.getSubCategoriesNames(ButtonTag))
            {
                PodkategorieCBox.Items.Add(podkategoria);
            }
        }

        private void nazwaBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (!Char.IsDigit((char)KeyInterop.VirtualKeyFromKey(e.Key)) & e.Key != Key.OemComma | e.Key == Key.OemPeriod)
            {
                e.Handled = true;
            }
        }

        private void InitTextBox(WrapPanel name)
        {
            TextBox nazwaBox = new TextBox();
            nazwaBox.Width = 100;
            TextBox cenaBox = new TextBox();
            cenaBox.Width = 55;
            cenaBox.KeyDown += nazwaBox_KeyDown;
            TextBox rozmiarBox = new TextBox();
            rozmiarBox.Width = 60;
            name.Children.Add(nazwaBox);
            name.Children.Add(cenaBox);
            name.Children.Add(rozmiarBox);
         }
        private void AddProduct_Click(object sender, RoutedEventArgs e)
        {
            WrapPanel dodawaniePanel = new WrapPanel();
            Label jakaKategoria2=new Label();
            jakaKategoria2.Content = ButtonTaglokalny;
            ComboBox Podkategorie2CBox = new ComboBox();
            Podkategorie2CBox.Width = 95;
            foreach (string podkategoria in dbFunctions.getSubCategoriesNames(ButtonTaglokalny))
            {
                Podkategorie2CBox.Items.Add(podkategoria);
            } 
            dodawaniePanel.Children.Add(jakaKategoria2);
            dodawaniePanel.Children.Add(Podkategorie2CBox);
            dodawanieStackPanel.Children.Add(dodawaniePanel);
            InitTextBox(dodawaniePanel);        
        }
      
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            string temp = "";
            foreach (WrapPanel dziecko in dodawanieStackPanel.Children)
            {
                if(((ComboBox)dziecko.Children[1]).SelectionBoxItem.ToString() != "")
                {
                    temp += ((TextBox)dziecko.Children[2]).Text + ";";
                    temp += ((Label)dziecko.Children[0]).Content + ";";
                    temp += ((ComboBox)dziecko.Children[1]).SelectedItem.ToString() + ";";
                    temp += ((TextBox)dziecko.Children[3]).Text + ";";
                    temp += ((TextBox)dziecko.Children[4]).Text + "\n";
                }
            }
            main.menu += temp;
            this.Close();
        }

        public void PodkategorieCBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            kolejnaPozycjaButton.IsEnabled = true;
            if (!isDone)
            {   
                podkategoria = PodkategorieCBox.SelectedItem.ToString();
                InitTextBox(dodawanePozycje);
                isDone = true;
            }
            else
                MessageBox.Show("Właśnie zmieniłeś podkategorię!");
        }
    }    
}
