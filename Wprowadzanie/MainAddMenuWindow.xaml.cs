﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Wprowadzanie;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for MainAddMenuWindow.xaml
    /// </summary>
    public partial class MainAddMenuWindow : Window
    {
        public string ButtonTag;
        public string lokal="";
        public string menu = "";
        private string filename = "";
        private string addedPositions = "";
        private string currentPositions = "";
        DatabaseFunctions dbFunctions = new DatabaseFunctions();
        public MainAddMenuWindow(string login)
        {
            InitializeComponent();
            textbox1.IsEnabled = false;
            this.MaxHeight = 500;
            this.MaxWidth = 500;
            userLogin.Content = login;
            foreach (System.Data.DataRow local in dbFunctions.returnUserPlaces(login).Rows)
            {
                cbCombo.Items.Add(local[0].ToString());
            }
        }
        
        public void copyString(string prtMenu)
        {
           menu = prtMenu;
        }
        private void WybPlik_Button(object sender, RoutedEventArgs e)
        {
            if (lokal.Length > 0)
            {
                Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                dlg.DefaultExt = ".txt";
                dlg.Filter = "TXT Files (*.txt)|*.txt";
                Nullable<bool> result = dlg.ShowDialog();

                if (result == true)
                {
                    filename = dlg.FileName;
                    textbox1.Text = filename;
                }
            }
            else
            {
                MessageBox.Show("Wybierz lokal!");
            }
        }

        private void Button_Zatw_Click(object sender, RoutedEventArgs e)
        {
            if (filename != "")
            {
                string menu = System.IO.File.ReadAllText(filename);
                foreach (string position in menu.Split(new Char[] { '\r', '\n' }))
                {
                    if (position != "")
                        dbFunctions.insertValue(position, dbFunctions.returnLocalID(lokal, dbFunctions.returnUserPlaces(userLogin.Content.ToString()).Rows[0][1].ToString()));
                }
            }
            else
            {
                if (menu != "")
                {
                    foreach (string position in menu.Split(new Char[] { '\n' }))
                    {
                        if (position != "")
                            dbFunctions.insertValue(position, dbFunctions.returnLocalID(lokal, dbFunctions.returnUserPlaces(userLogin.Content.ToString()).Rows[0][1].ToString()));
                    }
                }
            }
            if (lokal.Length > 0)
            {
                MySQLConnector connector = new MySQLConnector();
                addedPositions = (Int32.Parse(connector.Select("select count(id) from dishes where id_place=(select id from place where name='" + cbCombo.SelectionBoxItem.ToString() + "')")[0])-Int32.Parse(currentPositions)).ToString();
                MenuAfter zatwierdzoneMenu = new MenuAfter(lokal,addedPositions);
                zatwierdzoneMenu.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Wybierz lokal!");
            }
        }

        private void Button__AddProdGr_Click(object sender, RoutedEventArgs e)
        {
            if (lokal.Length > 0)
            {
                Button oButton = (Button)sender; //przycisk
                ButtonTag = oButton.Tag.ToString();
                AddProductGroup fragmentMenu = new AddProductGroup(ButtonTag, lokal,this);
                fragmentMenu.Show();
            }
            else
            {
                MessageBox.Show("Wybierz lokal!");
            }
        }

        private void cbCombo_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            lokal = cbCombo.SelectedItem.ToString();
            int last=lokal.LastIndexOf(":");
            lokal = lokal.Substring(last +1 );
            cbCombo.IsEnabled = false;
            MySQLConnector connection = new MySQLConnector();
            try
            {
                currentPositions = connection.Select("select count(id) from dishes where id_place=(select id from place where name='"+cbCombo.SelectionBoxItem.ToString()+"')")[0];
            }
            catch
            {
                currentPositions = "0";
            }
        }
        private void showInstruction(object sender, RoutedEventArgs e)
        {
            System.Diagnostics.Process.Start("notepad.exe", System.IO.Directory.GetCurrentDirectory() + "/../../instrukcja.txt");
        }
    }    
}
