﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Wprowadzanie
{
    /// <summary>
    /// Interaction logic for MenuAfter.xaml
    /// </summary>
    public partial class MenuAfter : Window
    {
        MySQLConnector connection = new MySQLConnector();   
        public MenuAfter(string lokal, string addedPositions)
        {
            InitializeComponent();
            InitBinding();
            MaxHeight = 500;
            MaxWidth = 500;
            lokalLabel.Content = lokal;
            logoImg.Source = new BitmapImage(new Uri(System.IO.Directory.GetCurrentDirectory() + "/../../Images/logo.png", UriKind.Absolute));
            System.Data.DataTable table = new System.Data.DataTable();
            connection.DataAdapter(table, "select category.name,subcategory.name,dishes.name,dishes.price,dishes.size from category,subcategory,dishes,place where dishes.id_category = category.id and id_subcategory = subcategory.id and id_place=(select id from place where name='" + lokalLabel.Content + "') group by dishes.name order by id desc limit "+addedPositions+"");
            lstMenu.ItemsSource = table.DefaultView;
        }

        public void InitBinding()
        {
        }
    }
}
